const fs = require("fs");
const sqlite3 = require('sqlite3');

module.exports.rebuildDB = function(data) {
        //creazione del DB degli itinerari..
        var db = new sqlite3.Database('db_id3king.sqlite3');
        debugger;

        db.serialize(function(){

          db.run("BEGIN TRANSACTION");
          db.run("drop table if exists 'LOCALITA'");
          db.run(`CREATE TABLE 'LOCALITA' (
                	'ID' INTEGER NOT NULL,
                	'Nome' TEXT NOT NULL,
                	PRIMARY KEY(ID))`);

         db.run("drop table if exists 'ITINERARIO'");
         db.run(`CREATE TABLE 'ITINERARIO' (
                	'ID'	INTEGER NOT NULL,
                	'Data'	TEXT NOT NULL,
                	'Link'	TEXT,
                	'Descrizione'	TEXT,
                	'Difficolta'	NUMERIC NOT NULL,
                	'Durata'	INTEGER,
                	'Lunghezza'	INTEGER,
                	'Dislivello'	REAL,
                	'ID_LOCALITA'	INTEGER,
                	FOREIGN KEY('ID_LOCALITA') REFERENCES LOCALITA(ID))`);

          db.run("drop table if exists 'TOPONIMO'");
          db.run(`CREATE TABLE 'TOPONIMO' (
                'ID' INTEGER NOT NULL,
                'Nome' TEXT,
                'Descrizione'	TEXT,
                 PRIMARY KEY(ID))`);

          db.run("drop table if exists 'ITINERARIO_TOPONIMO'");
          db.run(`CREATE TABLE 'ITINERARIO_TOPONIMO' (
                    ID_ITINERARIO INTEGER REFERENCES ITINERARIO (ID),
                    ID_TOPONIMO INTEGER REFERENCES TOPONIMO (ID),
                    PRIMARY KEY (ID_ITINERARIO, ID_TOPONIMO))`);

          //db.parallelize? https://github.com/mapbox/node-sqlite3/wiki/Control-Flow
          var stmt = db.prepare("INSERT INTO LOCALITA (ID, Nome) VALUES (?, ?)");
          data.localita.forEach(function(localita){
              stmt.run(localita.id, localita.nome);
          });
          stmt.finalize();

          stmt = db.prepare("INSERT INTO ITINERARIO (ID, Data, Link, Descrizione, Difficolta, Durata, Lunghezza, Dislivello, ID_LOCALITA) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")
          for (var chiave in data.itinerari) {
            var itinerario = data.itinerari[chiave];
            if(data.itinerari.hasOwnProperty(chiave))
                stmt.run(itinerario.id, itinerario.data, itinerario.link, itinerario.descrizione, itinerario.difficolta, itinerario.durata, itinerario.lunghezza, itinerario.dislivello, itinerario.IDlocalita);
          }
          stmt.finalize();

          stmt = db.prepare("INSERT INTO TOPONIMO (ID, Nome, Descrizione) VALUES (?, ?, ?)")
          data.toponimi.forEach(function(toponimo){
              stmt.run(toponimo.id, toponimo.nome, toponimo.descrizione);
          });
          stmt.finalize();

          stmt = db.prepare("INSERT INTO ITINERARIO_TOPONIMO (ID_ITINERARIO, ID_TOPONIMO) VALUES (?, ?)")
          data.toponimi.forEach(function(toponimo){
              toponimo.itinerari.forEach(function(itinerario){
                  stmt.run(itinerario, toponimo.id);
              });
          });
          stmt.finalize();
          db.run("END");
          console.log('builded');
        });
};

module.exports.getValues = function(){
    var db = new sqlite3.Database('db_id3king.sqlite3');
    db.all(`SELECT ITINERARIO.ID, Data, Link, ITINERARIO.Descrizione, Difficolta, Durata, Lunghezza, Dislivello, LOCALITA.Nome AS NomeLocalita, ID_ITINERARIO, group_concat(ID_TOPONIMO) as Toponimi
                FROM ITINERARIO INNER JOIN LOCALITA ON ITINERARIO.ID_LOCALITA = LOCALITA.ID
                JOIN ITINERARIO_TOPONIMO ON ITINERARIO.ID = ID_ITINERARIO
                JOIN TOPONIMO ON ITINERARIO_TOPONIMO.ID_TOPONIMO = TOPONIMO.ID
    			      Group by ID_ITINERARIO ORDER BY ID_ITINERARIO`, function(err, itinerari) {
              db.all(`SELECT ID, Nome, Descrizione
                      FROM TOPONIMO
                      ORDER BY ID`, function(err, toponimi) {
                          module.exports.values = {itinerari: itinerari, toponimi: toponimi};
                      });
           });
}

module.exports.dbExist = function(){
    return fs.existsSync('db_id3king.sqlite3');
}

/*SELECT ITINERARIO.ID, Data, Link, ITINERARIO.Descrizione, Difficolta, Durata, Lunghezza, Dislivello, LOCALITA.Nome AS NomeLocalita, ID_ITINERARIO, group_concat(ID_TOPONIMO) as Toponimi
            FROM ITINERARIO INNER JOIN LOCALITA ON ITINERARIO.ID_LOCALITA = LOCALITA.ID
            JOIN ITINERARIO_TOPONIMO ON ITINERARIO.ID = ID_ITINERARIO
            JOIN TOPONIMO ON ITINERARIO_TOPONIMO.ID_TOPONIMO = TOPONIMO.ID
			Group by ID_ITINERARIO ORDER BY ID_ITINERARIO*/
